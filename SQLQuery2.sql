drop procedure date_q1 
 
GO
CREATE PROCEDURE date_q1
@userEnteredDate varchar(12) , @userEnteredTime varchar(12)
AS
	SELECT
	--Volume Weighted Price
	SUM([close]*CONVERT(float,vol))/SUM(CONVERT(float,vol)) AS VOLUME_WEIGHTED_PRICE 

	--Start time and end time
	,right(left([date],8),2)+'/'+right(left([date],6),2)+'/'+left([date],4) As [Date],
	'Start: (' +case when CONVERT(int,@userEnteredTime)<10 then '0' +@userEnteredTime+ ':00)' else @userEnteredTime + ':00)' END AS 'Start', 
	'End: (' +case when CONVERT(int,@userEnteredTime)+5>23 then '00:00)' else CONVERT(varchar(50), CONVERT(int,@userEnteredTime)+5)+':00)' END AS 'End'

FROM [Challenges].[dbo].[Q1]
WHERE left([date],8) = @userEnteredDate  and CONVERT(int,left(right([date],4),2)) between CONVERT(int,@userEnteredTime) and (CONVERT(int,left(right([date],4),2)))+5
GROUP BY [date]
GO

--Execute
exec date_q1 @userEnteredDate = '20101011' , @userEnteredTime = 16
