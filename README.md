Emre Akarsu

--Challenge 1--
VWAP was calculated using the close price * volume / volume. Initially volume was set to type 'varchar' and had to be converted to 'float' to preform calculations. Fields set to decimal which contained decimal places.

Field name declarations:
''' CREATE TABLE [dbo].[Q1](
	[ticker] [varchar](4) NOT NULL,
	[date] [varchar](12) NOT NULL,
	[open] [decimal](8, 4) NOT NULL,
	[high] [decimal](8, 4) NOT NULL,
	[low] [decimal](8, 4) NOT NULL,
	[close] [decimal](8, 4) NOT NULL,
	[vol] [varchar](7) NOT NULL
) ON [PRIMARY] '''

To execute, run procedure 'date_q1'

exec date_q1 @userEnteredDate = '20101011' , @userEnteredTime = '4'



--Challenge 2--

'''CREATE TABLE [dbo].[sample_dataset3](
	[Date] [varchar](10) NOT NULL,
	[Time] [float] NOT NULL,
	[Open] [decimal](5, 2) NOT NULL,
	[High] [decimal](5, 2) NOT NULL,
	[Low] [decimal](5, 2) NOT NULL,
	[Close] [decimal](5, 2) NOT NULL,
	[Volume] [float] NULL
) ON [PRIMARY]'''

To execute challenge2, run procedure 'sql_q2'

exec SQL_Q2