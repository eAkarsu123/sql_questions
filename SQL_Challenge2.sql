CREATE PROCEDURE SQL_Q2
AS
	--Table1
	SELECT DISTINCT TOP 3 [Date], abs([open]-[close]) AS Range
	INTO ##Difference
	FROM sample_dataset3
	ORDER BY Range desc

	--Table2 maximum price for each of the 3 dates
	SELECT DISTINCT TOP 3 [Date] AS Date, max([high]) AS maxHigh
	INTO ##maxHighPrice
	FROM sample_dataset3
	WHERE [Date] in (SELECT Date FROM ##Difference)
	GROUP BY [Date]
	order by maxHigh DESC

	--Table3 Get the time at which the high occurs for the dates
	SELECT DISTINCT TOP 3 sample_dataset3.[date] as Date, [time] as TimeMax
	INTO ##highTime
	FROM sample_dataset3, ##maxHighPrice
	WHERE sample_dataset3.[high] = maxHigh and sample_dataset3.[date] = ##maxHighPrice.[date]
	GROUP BY sample_dataset3.[date], [time]
	ORDER BY Date Asc

	-- Join the 1st and 3rd Temp Tables together and print the relevant info
	SELECT t1.[Date] as Date,  t2.Range, t1.[TimeMax] as 'Max Price Time'
	FROM ##highTime t1 left join  ##Difference t2 on
	t1.[date] = t2.[date]

